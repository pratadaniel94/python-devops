### Commands

python3 -m venv venv

source venv/bin/activate

pip install poetry

poetry export -f requirements.txt -o requirements.txt

pip install -r requirements.txt


## install docker
curl https://get.docker.com | bash # script instalação docker linux

usermod -aG docker seuusuario  # adiciona permissão ao seu usuario no grupo docker

git checkout -b nomedabranch

docker run -dti imagem bash

docker-compose build
docker-compose up





