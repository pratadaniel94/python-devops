from flask import Blueprint
from flask import jsonify
from flask import request

from infra.mongo_client import mongo_connection

clients = Blueprint("clientes", __name__)

db = mongo_connection()


@clients.route("/clientes", methods=["GET"])
def get_client():
    return jsonify([x["name"] for x in db.clients.find()])


@clients.route("/clientes/<name>", methods=["GET", "POST"])
def get_clients(name):
    if request.method == "GET":
        client = db.clients.find_one({"name": name})
        client["_id"] = str(client["_id"])
        return jsonify(client)
    if request.method == "POST":
        client = request.get_json()
        client["name"] = name
        db.clients.insert_one(client)
        return jsonify({"msg": "cliente cadastrado com sucesso"}), 201


@clients.route("/clientes/<name>", methods=["DELETE"])
def delete_client(name: str) -> jsonify:
    try:
        db.clients.delete_one({"name": name})
        return jsonify({"msg": True})

    except Exception:
        return jsonify({"msg": False})


@clients.route("/clientes/<name>", methods=["PUT"])
def update_client(name):
    try:
        data = request.get_json()
        data["name"] = name
        db.clients.update_one({"name": name}, {"$set": data})
        return jsonify({"msg": True})
    except Exception:
        return jsonify({"msg": False})
