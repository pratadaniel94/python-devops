from bson import ObjectId
from flask import Blueprint
from flask import jsonify
from flask import request

from auth.auth import token_required
from infra.mongo_client import mongo_connection

db = mongo_connection()


products = Blueprint("products", __name__, url_prefix="/produtos")


@products.route("", methods=["GET"])
@token_required
def get_produts():
    products = db.products.find()
    aux = []
    for product in products:
        product["_id"] = str(product["_id"])
        aux.append(product)
    return jsonify(aux)


@products.route("/<id>", methods=["GET"])
@token_required
def get_produt(id):
    product = db.products.find_one({"_id": ObjectId(id)})
    product["_id"] = str(product["_id"])
    return jsonify(product)


@products.route("", methods=["POST"])
@token_required
def insert_product():
    product = request.get_json()  # Body da requisição
    if (
        "name" in product.keys() and "price" in product.keys()
    ):  # validação  de body request!
        db.products.insert_one(product)
        return jsonify({"msg": "produto cadastrado com sucesso"}), 201


@products.route("/<id>", methods=["DELETE"])
@token_required
def delete_product(id):
    db.products.delete_one({"_id": ObjectId(id)})
    return jsonify({"msg": "produto deletado com sucesso"})


@products.route("/<id>", methods=["PUT"])
@token_required
def update_products(id):
    update_product = request.get_json()
    db.products.update_one({"_id": ObjectId(id)}, {"$set": update_product})
    return jsonify({"msg": "produto atualizado com sucesso"})
