from bson import ObjectId
from flask import Blueprint
from flask import jsonify

from auth.auth import token_required
from infra.mongo_client import mongo_connection

db = mongo_connection()

users = Blueprint("usuarios", __name__)


@users.route("/users/<id>", methods=["GET"])
@token_required
def get_client(id):
    user = db.users.find_one({"_id": ObjectId(id)})
    user["_id"] = str(user["_id"])
    return jsonify(user)
