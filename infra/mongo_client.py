from pymongo import MongoClient


def mongo_connection():
    try:
        con = MongoClient("mongodb://root:example@200.100.50.3:27017/")
        db = con["project"]
        return db
    except Exception:
        raise ConnectionError("Falha ao conectar com o mongodb.")
