from functools import wraps

import jwt

from flask import current_app
from flask import jsonify
from flask import request


def token_required(fn):
    @wraps(fn)
    def decorated_function(*args, **kwargs):
        try:
            token = request.headers.get("Authorization").split()[-1]
            jwt.decode(
                token, current_app.config.get("JWT_SECRET"), algorithms=["HS256"]
            )
        except jwt.exceptions.ExpiredSignatureError:
            return jsonify({"ACK": False, "erro": "Assinatura de token expirada"}), 401
        except jwt.exceptions.InvalidSignatureError:
            return jsonify({"ACK": False, "erro": "Assinatura de token inválida"}), 400
        except jwt.exceptions.DecodeError:
            return jsonify({"ACK": False, "erro": "Erro ao decodificar o token"}), 400
        return fn(*args, **kwargs)

    return decorated_function
