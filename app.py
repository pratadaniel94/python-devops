from flask import Flask
from flask import render_template
from flask import request

from auth.auth import token_required
from recursos.clientes import clients
from recursos.produtos import products
from recursos.usuarios import users

app = Flask(__name__)
app.register_blueprint(clients)
app.register_blueprint(products)
app.register_blueprint(users)
app.config["PRIVATE_KEY"] = "45aaef13-87f3-4a58-8eaf-43aff1e68871"
app.config["JWT_SECRET"] = "#qdsv$%TAdgTredh4@#%g"


@app.route("/", methods=["GET", "POST"])
@token_required
def index():
    if request.method == "GET":
        context = {"title": "titulo da aplicação"}
        return render_template("index.html", context=context, sequencia=list(range(10)))
    elif request.method == "POST":
        return "post return", 201


@app.route("/teste")
@token_required
def index_test():
    return "nova rota"


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
